<?php
/**
 * Services
 *
 * @package   Login\Controllers
 * @author    SISA Dev Team
 * @copyright 2021 SISA
 * @license   https://creativecommons.org/licenses/by-nc-nd/4.0/ (CC BY-NC-ND 4.0)
 */

namespace Modules\Login\Controllers;

use App\Controllers\BaseController;
use Modules\Login\Libraries\Auth;

/**
 * Services
 *
 * @package   Login\Controllers
 * @author    SISA Dev Team
 * @copyright 2021 SISA
 * @license   https://creativecommons.org/licenses/by-nc-nd/4.0/ (CC BY-NC-ND 4.0)
 */
class Login extends BaseController
{
	/**
	 * Index
	 * Handles index page
	 *
	 * @return view Views corresponding to either index page or logout
	 */
	public function index()
	{
		$session = session();
		helper('cookie');

		$cookie = $this->request->getCookie('sisa_token');

		if (null !== $cookie)
		{
			$session->set('access_token', $cookie);
		}
		else
		{
			return $this->logout();
		}

		if ($session->has('access_token') && null !== $session->get('access_token'))
		{
			set_cookie('sisa_token', $session->get('access_token'));
			$session      = session();
			$token        = $session->get('access_token');
			$data['user'] = $this->getUserData($token);

			return view(trim('\Modules\Login\Views\Login\home'), $data);
		}
		else
		{
			echo('pas de session');
			return $this->logout();
		}
	}

	/**
	 * SelectAuth
	 * Handles auth choice
	 *
	 * @return view View of choice
	 */
	public function selectAuth()
	{
		helper('cookie');
		set_cookie('sisa_token', '', time() - 3600);
		helper('form');
		return view(trim('\Modules\Login\Views\Login\choice'));
	}

	/**
	 * GetUserData
	 * Get user info with token via API call
	 *
	 * @param string $token User token
	 *
	 * @return array $user User infos
	 */
	public function getUserData(string $token)
	{
		$auth = new Auth();

		$request = $auth->getUserFromToken($token);

		$user = [

			'email'     => $request['email'],
			'firstname' => $request['firstname'],
			'lastname'  => $request['lastname'],
		];

		$data['user'] = $user;

		return $user;
	}

	/**
	 * Connect
	 * Handles login with type
	 *
	 * @param string $type Type of connection
	 *
	 * @return view Corresponding view according to type
	 */
	public function connect(string $type)
	{
		helper('form');
		$data['type'] = $type;
		return view(trim('\Modules\Login\Views\Login\login_' . $type), $data);
	}

	/**
	 * Handle
	 * Handles form validation & cookie from login page
	 *
	 * @return view Corresponding view or redirection
	 */
	public function handle()
	{
		$type    = $this->request->getPostGet('type');
		$setPost = (null !== $this->request->getPost()) ? true : false;
		if ($this->request->getPost('email') && $this->request->getPost('password'))
		{
			helper('form');
			$restrictions = [
				'email'    => [
					'rules'  => 'required|valid_email',
					'errors' => [
						'valid_email' => 'Adresse e-mail non valide.',
						'required'    => 'Adresse e-mail requise.',
					],
				],
				'password' => [
					'rules'  => 'required|regex_match[/^\S*(?=\S{8})(?=\S*[a-z])(?=\S*[A-Z])(?=\S*[\W])(?=\S*[\d])\S*$/]',
					'errors' => [
						'required'    => 'Mot de passe requis.',
						'regex_match' => 'Le mot de passe doit contenir au moins 1 majuscule, 8 caractères, et un caractère spécial.',
					],
				],
			];
			if ($this->validate($restrictions))
			{
				$auth = new Auth();

				$data['email']    = $this->request->getPost('email');
				$data['password'] = $this->request->getPost('password');

				$token = $auth->getTokenFromUser($data);

				$tokenRestrictions = [

					'access_token' => [
						'rules'  => 'required',
						'errors' => [
							'required' => 'Utilisateur introuvable',

						],

					],
				];
				if (null !== $token['access_token'])
				{
					$session                     = session();
					$sessionData['access_token'] = $token['access_token'];

					$session->set($sessionData);
					helper('cookie');
					set_cookie('sisa_token', $token['access_token']);
					return redirect()->to('login')->withCookies();
				}
				else
				{
					$data['validation'] = $this->validator;
					return view(trim('\Modules\Login\Views\Login\login_' . $type), $data);
				}
			}
			else
			{
				$data['validation'] = $this->validator;
				return view(trim('\Modules\Login\Views\Login\login_' . $type), $data);
			}
		}

		else
		{
			helper('form');
			return view(trim('\Modules\Login\Views\Login\login_' . $type), $data);
		}
	}

	/**
	 * Logout
	 *
	 * @return redirect A redirect to the auth page
	 */
	public function logout()
	{
		$session = session();
		$session->destroy();

		helper('cookie');
		set_cookie('sisa_token', '', time() - 3600);
		return redirect()->to('login/auth');
	}
}
