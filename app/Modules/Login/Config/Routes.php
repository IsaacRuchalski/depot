<?php
/**
 * Routes
 *
 * @package   Users\Config
 * @author    SISA Dev Team
 * @copyright 2021 SISA
 * @license   https://creativecommons.org/licenses/by-nc-nd/4.0/ (CC BY-NC-ND 4.0)
 */

namespace Modules\Login\Config;

$routes->group(
	'login', ['namespace' => 'Modules\Login\Controllers'], function ($routes) {
		$routes->addRedirect('', 'login/index');
		$routes->match(['get', 'post'], 'index', 'Login::index');
		$routes->match(['get', 'post'], 'select', 'Login::selectAuth');
		$routes->match(['get', 'post'], 'auth', 'Login::selectAuth');
		$routes->match(['get', 'post'], 'handle', 'Login::handle');
		$routes->match(['get', 'post'], 'connect/(:segment)', 'Login::connect/$1');
		$routes->match(['get', 'post'], 'logout', 'Login::logout');
	}

);
