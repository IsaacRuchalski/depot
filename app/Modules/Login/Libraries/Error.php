<?php
/**
 * Error
 *
 * @package   Login\Libraries
 * @author    SISA Dev Team
 * @copyright 2021 SISA
 * @license   https://creativecommons.org/licenses/by-nc-nd/4.0/ (CC BY-NC-ND 4.0)
 */

namespace Modules\Login\Libraries;

/**
 * Error
 *
 * @package   Login\Libraries
 * @author    SISA Dev Team
 * @copyright 2021 SISA
 * @license   https://creativecommons.org/licenses/by-nc-nd/4.0/ (CC BY-NC-ND 4.0)
 */
class Error
{

	/**
	 * GenerateError
	 *
	 * @param string $error The Error text
	 *
	 * @return string The HTML element containing the error
	 */
	public function generateError(string $error)
	{
				$html  = '';
				$html .= '<div class="alert alert-danger mt-20" role="alert">';
				$html .= '<h4 class="alert-heading">Erreur</h4>';
				$html .= $error;
				$html .= '</div>';

				return $html;
	}

}
