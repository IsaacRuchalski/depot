<?php
/**
 * Auth
 *
 * @package   Login\Libraries
 * @author    SISA Dev Team
 * @copyright 2021 SISA
 * @license   https://creativecommons.org/licenses/by-nc-nd/4.0/ (CC BY-NC-ND 4.0)
 */

namespace Modules\Login\Libraries;

/**
 * Auth
 *
 * @package   Login\Libraries
 * @author    SISA Dev Team
 * @copyright 2021 SISA
 * @license   https://creativecommons.org/licenses/by-nc-nd/4.0/ (CC BY-NC-ND 4.0)
 */
class Auth
{

	/**
	 * Domain
	 * Domain identifier on the API
	 *
	 * @var string
	 */
	public $domain = 'gnlpLMzWwZ';
	/**
	 * GetUserFromToken
	 * Uses APi calls to get user credentials from access token
	 *
	 * @param string $token User token
	 *
	 * @return array $obj Array containing user data
	 */
	public function getUserFromToken(string $token)
	{
		$client = \Config\Services::curlrequest();

		$response = $client->request('GET', 'https://erp.sisa.ovh/api/user', [
			'verify' => false,
			'query'  => [
				'access_token' => $token,
				'app'          => $this->domain,
			],

		]);

		$obj = json_decode($response->getBody(), true);

		return $obj;
	}

	/**
	 * GetTokenFromUser
	 * Uses API call to get token from user credentials
	 *
	 * @param array $data User credentials (mail & password)
	 *
	 * @return array $obj Array containing token and token access timestamp
	 */
	public function getTokenFromUser(array $data)
	{
		$email    = $data['email'];
		$password = $data['password'];
		$client   = \Config\Services::curlrequest();

		$response = $client->request('GET', 'https://erp.sisa.ovh/api/token', [
			'verify'      => false,
			'http_errors' => false,
			'query'       => [
				'email'    => $email,
				'password' => $password,
				'app'      => $this->domain,
			],
		]);

		$obj = json_decode($response->getBody(), true);

		$code = $response->getStatusCode();
		//case of non-200 HTTP code
		if (! preg_match('/^(?:2)\d+$/', $code))
		{
			$obj['access_token'] = null;
		}
			return $obj;
	}
}
