<?= $this->extend('../Modules/Templates/home') ?>

<?= $this->section('navbar') ?>

<div class="dropdown ml-auto">
   <button class="btn alt-dm" data-toggle="dropdown" type="button" id="dropdown-toggle-btn-1" aria-haspopup="true" aria-expanded="false">
   Profil
   </button>
   <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdown-toggle-btn-1">
	  <h4 class="dropdown-header"><?= $user['firstname'] . ' ' . $user['lastname'] ?></h4>
	  <h6 class="dropdown-header"><?= $user['email'] ?></h6>
	  <a href="#" class="dropdown-item">  <span class="material-icons align-middle">person</span> Mon profil</a>
	  <a href="#" class="dropdown-item"> <span class="material-icons align-middle">settings</span> Paramètres</a>
	  <div class="dropdown-divider"></div>
	  <div class="dropdown-content">
		 <a href = "logout"><button class="btn btn-square btn-primary" type="button"><span class="material-icons d-inline-flex justify-content-center">logout</span></button></a>
		 <button class="btn btn-square btn-primary" type="button" onclick="halfmoon.toggleSidebar()"><span class="material-icons d-inline-flex justify-content-center">menu</span></button>
		 <button class="btn btn-square btn-primary" type="button" onclick="halfmoon.toggleDarkMode()"><span class="material-icons-outlined d-inline-flex">dark_mode</span></button>
	  </div>
   </div>
</div>

<?= $this->endSection() ?>

<?= $this->section('content')?>

<h1> </h1>

<?= $this->endSection() ?>