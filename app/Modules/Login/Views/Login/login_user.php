<?= $this->extend('../Modules/Templates/login') ?>

<?= $this->section('navbar') ?>

<?= $this->endSection() ?>

<?= $this->section('content')?>

<div class = "content-wrapper d-flex justify-content-center w-full h-full">
   <div class = "align-self-center w-600 mw-full bg-dark-dm p-20 border rounded">
	  <?php
	  $hidden = ['type' => 'user'];
		 echo form_open('login/handle', '', $hidden) ?>
	  <div class="form-group">
		 <label for="email" class="required">Email</label>
		 <input type="text" name='email' id="email" class="form-control" placeholder="email">
	  </div>

	  <div class="form-group">
		 <label for="password" class="required">Password</label>
		 <input type="password" name='password' id="password" class="form-control" placeholder="password">
	  </div>
	  <input class="btn btn-primary btn-block" type="submit" value="Se connecter">
	
	  </form>
	  
	  <div class = "text-left pt-20">
		 <a href = "/login/forgotpassword">Mot de passe oublié</a>

	  </div>
	</form>
	<?= $this->include('../Modules/Login/Views/errorAlert') ?>
   </div>
</div>

<?= $this->endSection() ?>