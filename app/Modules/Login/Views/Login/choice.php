<?= $this->extend('../Modules/Templates/login') ?>
<?= $this->section('navbar') ?>
<?= $this->endSection() ?>
<?= $this->section('content')?>
<div class = "content-wrapper d-flex justify-content-center w-full h-full">
   <div class = "align-self-center w-600 mw-full bg-dark-dm p-20 border rounded">

   <div class="collapse-group mw-full align-self-center">
      <details class="collapse-panel">
         <!-- w-400 = width: 40rem (400px), mw-full = max-width: 100% -->
         <summary class="collapse-header bg-white-lm">
            Connexion en tant qu'utilisateur
         </summary>
         <div class="collapse-content bg-light-lm">
            <a href = "connect/user">Se connecter</a>


         </div>
      </details>

	  <details class="collapse-panel">
         <!-- w-400 = width: 40rem (400px), mw-full = max-width: 100% -->
         <summary class="collapse-header bg-white-lm">
            Connexion en tant qu'administrateur
         </summary>
         <div class="collapse-content bg-light-lm">
            <a href = "connect/admin">Se connecter</a> <br> <i>La connexion s'effectuera depuis l'ERP SISA </i>
         </div>
      </details>
   </div>


</div>
<?= $this->endSection() ?>