<!DOCTYPE html>
<html lang="en">
<head>
  <!-- Meta tags -->
  <link href="https://cdn.jsdelivr.net/npm/halfmoon@1.1.1/css/halfmoon-variables.min.css" rel="stylesheet" />
  <link href="https://fonts.googleapis.com/css?family=Material+Icons|Material+Icons+Outlined|Material+Icons+Two+Tone|Material+Icons+Round|Material+Icons+Sharp" rel="stylesheet">
  <?= $this->renderSection('head') ?>
</head>
<body class="with-custom-webkit-scrollbars with-custom-css-scrollbars" data-dm-shortcut-enabled="true" data-sidebar-shortcut-enabled="true" data-set-preferred-mode-onload="true">
  <!-- Modals go here -->
  <!-- Reference: https://www.gethalfmoon.com/docs/modal -->

  <!-- Page wrapper start -->
  <div class="page-wrapper with-navbar">

    <!-- Sticky alerts (toasts), empty container -->
    <!-- Reference: https://www.gethalfmoon.com/docs/sticky-alerts-toasts -->
    <div class="sticky-alerts"></div>

    <!-- Navbar start -->
    <nav class="navbar bg-primary">
      <?= $this->renderSection('navbar') ?>
    </nav>
    <!-- Navbar end -->

    <!-- Content wrapper start -->
    <div class="content-wrapper">

	<?= $this->renderSection('content') ?>

	
    </div>
    <!-- Content wrapper end -->
	
  </div>
  <!-- Page wrapper end -->
  
  <!-- Halfmoon JS -->
  <?= $this->renderSection('JS') ?>
</body>
</html>
<script src="https://cdn.jsdelivr.net/npm/halfmoon@1.1.1/js/halfmoon.min.js"></script>
<style>



:root{

--primary-color: #539FB8;

--white-bg-color: #e6e6e6;

}

ul li {

list-style : none;

}

ul li p:before {}

.material-icons, .material-icons-outlined {

	line-height: unset;

}
</style>
