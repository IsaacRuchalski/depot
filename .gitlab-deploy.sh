#!/bin/bash
#Get servers list
#set -f
string=$DEPLOY_SERVER
array=(${string//,/ })
#Iterate servers for deploy and pull last commit
echo "ENV: $ENV"
echo "IP: $DEPLOY_SERVER"
for i in "${!array[@]}"
do    
	echo "Deploy project on server ${array[i]}"
	ssh -p 8022 deploy@${array[i]} "mkdir /home/deploy/${SERVICE}"
	rsync -az -e 'ssh -p 8022' --exclude='.git*' . deploy@${array[i]}:/home/deploy/${SERVICE}
	ssh -p 8022 deploy@${array[i]} << EOF
cd /home/deploy/${SERVICE}
composer update
cp /home/site/${SERVICE}/.env /home/deploy/${SERVICE}/.env
php spark migrate
mv /home/site/${SERVICE} /home/site/${SERVICE}_old && mv /home/deploy/${SERVICE} /home/site/${SERVICE}
rm -rf /home/site/${SERVICE}_old
chown -R deploy:www-data /home/site/${SERVICE}	
EOF
done
